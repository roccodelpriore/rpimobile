//
//  RLMPerson.h
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/27/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "RLMObject.h"

@interface RLMPerson : RLMObject

@end

//
//  Stop.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stop : NSObject
@property (nonatomic) NSString *idTag;
@property (nonatomic) NSNumber *latitude;
@property (nonatomic) NSNumber *longitude;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *shortName;
@property (nonatomic) NSNumber *stopNum;
@property (nonatomic) NSSet *routes;
@end

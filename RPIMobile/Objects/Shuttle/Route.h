//
//  Route.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Route : NSObject
@property (nonatomic) NSString *color;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *routeId;
@property (nonatomic) NSNumber *width;
@property (nonatomic) NSString *pointList;
@property (nonatomic) NSSet *shuttles;
@property (nonatomic) NSSet *stops;
@end

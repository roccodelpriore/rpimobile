//
//  RPIShuttle.m
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "RPIShuttle.h"

@interface RPIShuttle ()
@end

@interface UIImage (RotationMethods)
- (UIImage*)imageRotatedByDegrees:(CGFloat)degrees;
@end

@implementation RPIShuttle

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)copyStatusFromShuttle:(RPIShuttle*)shuttle
{
  self.direction = shuttle.direction;
  self.type = shuttle.type;
  self.identifier = shuttle.identifier;
  self.name = shuttle.name;
  self.statusMessage = shuttle.statusMessage;
  self.cardinalPoint = shuttle.cardinalPoint;
  self.updateTime = shuttle.updateTime;
  self.heading = shuttle.heading;
  self.coordinate = shuttle.coordinate;
}

- (CGFloat)degreesFromHeading
{
  NSLog(self.cardinalPoint);
  if ([self.cardinalPoint isEqualToString:@"East"]) {
    return 0;
  } else if ([self.cardinalPoint isEqualToString:@"North-East"]) {
    return -45;
  } else if ([self.cardinalPoint isEqualToString:@"North"]) {
    return -90;
  } else if ([self.cardinalPoint isEqualToString:@"North-West"]) {
    return -135;
  } else if ([self.cardinalPoint isEqualToString:@"West"]) {
    return -180;
  } else if ([self.cardinalPoint isEqualToString:@"South-West"]) {
    return -225;
  } else if ([self.cardinalPoint isEqualToString:@"South"]) {
    return -270;
  } else if ([self.cardinalPoint isEqual:@"South-East"]) {
    return -315;
  } else {
    return 0;
  }
}

- (MKAnnotationView*)annotationView
{
  MKAnnotationView* annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"ShuttleAnnotation"];
  
  annotationView.enabled = YES;
  annotationView.canShowCallout = YES;
//  annotationView.image = [[UIImage imageNamed:@"shuttle"] imageRotatedByDegrees:self.heading];
  annotationView.image = [[UIImage imageNamed:@"shuttle"] imageRotatedByDegrees: self.degreesFromHeading];
  
  return annotationView;
}

@end

@implementation UIImage (RotationMethods)

static CGFloat DegreesToRadians(CGFloat degrees) { return degrees * (M_PI / 180); };

- (UIImage*)imageRotatedByDegrees:(CGFloat)degrees
{
  // calculate the size of the rotated view's containing box for our drawing space
  UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
  CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
  rotatedViewBox.transform = t;
  CGSize rotatedSize = rotatedViewBox.frame.size;
  
  // Create the bitmap context
  UIGraphicsBeginImageContext(rotatedSize);
  CGContextRef bitmap = UIGraphicsGetCurrentContext();
  
  // Move the origin to the middle of the image so we will rotate and scale around the center.
  CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
  
  //   // Rotate the image context
  CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
  
  // Now, draw the rotated/scaled image into the context
  CGContextScaleCTM(bitmap, 1.0, -1.0);
  CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
  
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
  
}

@end

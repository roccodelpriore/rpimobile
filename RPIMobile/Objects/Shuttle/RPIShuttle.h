//
//  RPIShuttle.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

typedef enum {
    RPIShuttleDirectionNorth = 1,
    RPIShuttleDirectionSouth = 2,
    RPIShuttleDirectionWest = 3,
    RPIShuttleDirectionEast = 4
} RPIShuttleDirection;

typedef enum {
    RPIShuttleTypeWest = 1,
    RPIShuttleTypeEast = 2
} RPIShuttleType;

@interface RPIShuttle : NSObject <MKAnnotation>

//@property (nonatomic) NSNumber* routeId;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) RPIShuttleDirection direction;
@property (nonatomic) RPIShuttleType type;
@property (nonatomic) NSNumber* identifier;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* statusMessage;
@property (nonatomic) NSString* cardinalPoint;
@property (nonatomic) NSDate* updateTime;
@property (nonatomic) CGFloat heading;
@property (copy, nonatomic) NSString* title;

- (void)copyStatusFromShuttle:(RPIShuttle*)shuttle;
- (CGFloat)degreesFromHeading;
- (MKAnnotationView*)annotationView;

@end
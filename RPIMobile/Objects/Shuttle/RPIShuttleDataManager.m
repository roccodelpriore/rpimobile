//
//  RPIShuttleDataManager.m
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "RPIShuttleDataManager.h"
#import "RPIShuttle.h"
#import "Route.h"
#import "Stop.h"

@implementation RPIShuttleDataManager

+ (RPIShuttleDataManager *) sharedInstance {
    static dispatch_once_t pred;
    static RPIShuttleDataManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

//  Load the routes/stops from JSON asynchronously
- (void)loadRoutesAndStops:(NSMutableDictionary*) routesAndStops {
  if (!self.loadMapInfoJsonQueue) {
    self.loadMapInfoJsonQueue = dispatch_queue_create("rpimobile.jsonqueue", NULL);
  }
  
  dispatch_async(self.loadMapInfoJsonQueue, ^{
    NSError *error = nil;
    NSURL *routesStopsUrl = [NSURL URLWithString: RoutesAndStopsUrl];
    NSString *jsonString = [NSString stringWithContentsOfURL: routesStopsUrl
                                                    encoding: NSUTF8StringEncoding
                                                    error: &error];
    
    if (error) {
      NSLog(@"Error retrieving JSON data: %@", error);
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        [self parseRoutesandStopsFromJson:jsonString routesAndStops:routesAndStops];
        [[NSNotificationCenter defaultCenter] postNotificationName: RoutesAndStopsLoaded object:self];
      });
    }
  });
}

- (BOOL)parseRoutesandStopsFromJson:(NSString *)jsonString routesAndStops: (NSMutableDictionary*) routesAndStops {
  NSError *error = nil;
  NSData *data = [jsonString dataUsingEncoding: NSUTF8StringEncoding];
  NSString *string = nil;
  NSMutableArray *routes = [[NSMutableArray alloc] init];
  NSMutableArray *stops = [[NSMutableArray alloc] init];
  
  id jsonDict = [NSJSONSerialization JSONObjectWithData: data
                                     options: NSJSONReadingMutableLeaves
                                     error: &error];

  if (error != nil) {
    NSLog(@"Error: %@", [error description]);
    return NO;
  }
  
  if (jsonDict && [jsonDict isKindOfClass: [NSDictionary class]]) {
      NSDictionary *jsonRoutes = [jsonDict objectForKey:@"routes"];
      for (NSDictionary *value in jsonRoutes) {
        NSNumber *routeId = [value objectForKey:@"id"];
        
        Route *route = [[Route alloc] init];
        route.routeId = routeId;
        
        if (route) {
          // Set the route using the new info
          NSNumber *number = [value objectForKey:@"width"];
          route.width = number;
          
          string = [value objectForKey:@"name"];
          route.name = string;
          
          string = [value objectForKey:@"color"];
          route.color = string;
          
          NSDictionary *coordsDict = [value objectForKey:@"coords"];
          
          NSMutableString *pointsString = [NSMutableString string];
          for (NSDictionary *coordsValues in coordsDict) {
            [pointsString appendFormat:@"%@,%@;",
             [coordsValues objectForKey:@"latitude"],
             [coordsValues objectForKey:@"longitude"]];
          }
          
          string = [NSString stringWithString:pointsString];
          if ([string length] > 0) {
            string = [string substringToIndex:[string length] - 1];
          }
          
          route.pointList = string;
          
          [routes addObject: route];
        }
      }
    
      int stopNum = 0;
      NSDictionary *jsonStops = [jsonDict objectForKey:@"stops"];
      for (NSDictionary *value in jsonStops) {
        NSString *stopName = [value objectForKey:@"name"];
        
        Stop *stop = [[Stop alloc] init];
        stop.name = stopName;
        
        if (stop) {
          string = [value objectForKey:@"latitude"];
          stop.latitude = @([string doubleValue]);
          
          string = [value objectForKey:@"longitude"];
          stop.longitude = @([string doubleValue]);
          
          string = stopName;
          
          // Special handling for long stop names.
          if ([string isEqualToString:@"Blitman Residence Commons"]) {
            string = @"Blitman Commons";
          } else if ([string isEqualToString:@"Polytechnic Residence Commons"]) {
            string = @"Polytech Commons";
          } else if ([string isEqualToString:@"Troy Building Crosswalk"]) {
            string = @"Troy Bldg. Crossing";
          } else if ([string isEqualToString:@"6th Ave. and City Station"]) {
            string = @"6th Ave. & City Stn";
          }
          
          stop.shortName = string;
          
          string = [value objectForKey:@"short_name"];
          stop.idTag = string;
          
          stop.stopNum = @(stopNum++);
          
          NSDictionary *routesDict = [value objectForKey:@"routes"];
          NSMutableArray *tempRouteIds = [[NSMutableArray alloc] init];
          
          // Associate the stop with its routes
          for (NSDictionary *routeValues in routesDict) {
            NSNumber* number = [routeValues objectForKey:@"id"];
            [tempRouteIds addObject: number];
          }
          
          [stops addObject: stop];
        }
    
    }
    
    [routesAndStops setObject: routes forKey: @"routes"];
    [routesAndStops setObject: stops forKey: @"stops"];
    
    return YES;
  }
  
  return NO;
}

- (void)updateVehicleData {
  if (!self.loadVehicleJsonQueue) {
    self.loadVehicleJsonQueue = dispatch_queue_create("rpimobile.jsonqueue", NULL);
  }
  
  dispatch_async(self.loadVehicleJsonQueue, ^{
    NSError *error = nil;
    NSURL* shuttleJsonUrl = [NSURL URLWithString: CurrentShuttlesUrl];
    NSString *jsonString = [NSString stringWithContentsOfURL: shuttleJsonUrl
                                                    encoding: NSUTF8StringEncoding
                                                       error: &error];
    
    if (error) {
      NSLog(@"Error retrieving JSON data: %@", error);
    } else {
      NSMutableDictionary* shuttles = [[NSMutableDictionary alloc] init];
      [self parseShuttlesFromJson: jsonString shuttles:shuttles];
      
      [[NSNotificationCenter defaultCenter] postNotificationName:VehiclesUpdated
                                                          object:nil
                                                        userInfo:@{ @"shuttles" : shuttles }];
    }
  });
}

- (BOOL)parseShuttlesFromJson:(NSString*) jsonString shuttles:(NSMutableDictionary*) shuttles;
{
  if ([jsonString isEqualToString:@"null"]) {
    return NO;
  }
  
  NSError *error = nil;
  NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  id jsonArray = [NSJSONSerialization JSONObjectWithData: data
                                                 options: NSJSONReadingMutableLeaves
                                                   error: &error];
  
  if (error != nil) {
    NSLog(@"Error: %@", [error description]);
    return NO;
  }
  
  if (jsonArray && [jsonArray isKindOfClass:[NSArray class]]) {
    for (NSDictionary* shuttlesDict in jsonArray) {
      NSDictionary* shuttleDictionary = [shuttlesDict objectForKey:@"vehicle"];
      NSDictionary* positionDictionary = [shuttleDictionary objectForKey:@"latest_position"];
      
      // RFC 3339 date format
      NSString* dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
      NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
      
      NSNumber* identifier = [shuttleDictionary objectForKey:@"id"];
      
      RPIShuttle* shuttle = [shuttles objectForKey:identifier];
      if (!shuttle) {
        shuttle = [[RPIShuttle alloc] init];
        [shuttles setObject: shuttle forKey:[identifier stringValue]];
      }
      
      shuttle.identifier = identifier;
      shuttle.name = [shuttleDictionary objectForKey:@"name"];
      shuttle.heading = [[positionDictionary objectForKey:@"heading"] floatValue];
      shuttle.cardinalPoint = [positionDictionary objectForKey:@"cardinal_point"];
      shuttle.statusMessage = [positionDictionary objectForKey:@"public_status_msg"];
      shuttle.title = shuttle.name;
      
      double latitude = [[positionDictionary objectForKey:@"latitude"] doubleValue];
      double longitude = [[positionDictionary objectForKey:@"longitude"] doubleValue];
      shuttle.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
      
      dateFormatter.dateFormat = dateFormat;
      dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
      shuttle.updateTime = [dateFormatter dateFromString:[positionDictionary objectForKey:@"timestamp"]];
    }
  }
  
  return YES;
}

@end

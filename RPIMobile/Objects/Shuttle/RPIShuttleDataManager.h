//
//  RPIShuttleDataManager.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataUrls.h"

#define RoutesAndStopsLoaded @"RoutesAndStopsLoaded"
#define VehiclesUpdated @"VehiclesUpdated"

@interface RPIShuttleDataManager : NSObject

@property (nonatomic) dispatch_queue_t loadMapInfoJsonQueue;
@property (nonatomic) dispatch_queue_t loadVehicleJsonQueue;

+ (RPIShuttleDataManager*) sharedInstance;
- (void)loadRoutesAndStops:(NSMutableDictionary*) routesAndStops;
- (BOOL)parseRoutesandStopsFromJson:(NSString *)jsonString routesAndStops: (NSMutableDictionary*) routesAndStops;
- (void)updateVehicleData;
- (BOOL)parseShuttlesFromJson:(NSString *)jsonString shuttles: (NSMutableDictionary*) shuttles;

@end

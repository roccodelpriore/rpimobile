//
//  AppDelegate.m
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/27/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "AppDelegate.h"

//View Controllers
#import "RPIDirectoryViewController.h"
#import "RPIShuttleMapViewController.h"

//Navigation
#import "RPIMasterViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    RPIMasterViewController *master = [[RPIMasterViewController alloc] init];
    
    //Set the NavigationBar Text Color
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    
    //Set Up View Controllers
    RPIDirectoryViewController *directoryViewController = [[RPIDirectoryViewController alloc] initWithMaster:master];
    RPIShuttleMapViewController *shuttleViewController = [[RPIShuttleMapViewController alloc] initWithMaster:master];
    
    //Set Up Menu Objects
    RPIMasterMenuObject *directoryMenuObject = [[RPIMasterMenuObject alloc] initWithViewController:directoryViewController andTitle:@"Directory" andImage:nil];
    RPIMasterMenuObject *shuttleMenuObject = [[RPIMasterMenuObject alloc] initWithViewController:shuttleViewController andTitle:@"Shuttle" andImage:nil];
    
    //Set Up Navigation
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:shuttleViewController];
    master.menus = @[directoryMenuObject, shuttleMenuObject];
    master.navigation = navigation;
    
    //Show On Screen
    self.window.rootViewController = navigation;
    self.window.tintColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

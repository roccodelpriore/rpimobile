//
//  RMMasterViewController.h
//  Rollio Mobile
//
//  Created by Rocco Del Priore on 5/10/14.
//  Copyright (c) 2014 Rollio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPIMasterMenuObject.h"
#import "UIImage+ImageEffects.h"

@interface RPIMasterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate> {
    UIImageView *_backImageView;
    UIImageView *_frontImageView;
    UILabel *_titleLabel;
    UICollectionView *_collectionView;
    UICollectionViewFlowLayout *_layout;
    
    NSNumber *index;
}

@property (nonatomic) NSArray *menus;
@property (nonatomic) UINavigationController *navigation;

- (void)show;
- (id)initWithMenus:(NSArray *)menuObjects andNavigationController:(UINavigationController *)navigationController;

@end

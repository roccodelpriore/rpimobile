//
//  RMMasterViewController.m
//  Rollio Mobile
//
//  Created by Rocco Del Priore on 5/10/14.
//  Copyright (c) 2014 Rollio. All rights reserved.
//

#import "RPIMasterViewController.h"
#import "RPIMasterMenuCell.h"
#import "RPIMasterCollectionCell.h"
#import "RPIMasterCollectionLayout.h"

@interface RPIMasterViewController ()

@end

@implementation RPIMasterViewController
@synthesize menus, navigation;

- (UIImage *)screenshot
{
    CGSize imageSize = CGSizeZero;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        imageSize = [UIScreen mainScreen].bounds.size;
    } else {
        imageSize = CGSizeMake([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
    }
    
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    for (UIWindow *window in [[UIApplication sharedApplication] windows]) {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, window.center.x, window.center.y);
        CGContextConcatCTM(context, window.transform);
        CGContextTranslateCTM(context, -window.bounds.size.width * window.layer.anchorPoint.x, -window.bounds.size.height * window.layer.anchorPoint.y);
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            CGContextRotateCTM(context, M_PI_2);
            CGContextTranslateCTM(context, 0, -imageSize.width);
        } else if (orientation == UIInterfaceOrientationLandscapeRight) {
            CGContextRotateCTM(context, -M_PI_2);
            CGContextTranslateCTM(context, -imageSize.height, 0);
        } else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
            CGContextRotateCTM(context, M_PI);
            CGContextTranslateCTM(context, -imageSize.width, -imageSize.height);
        }
        if ([window respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
            [window drawViewHierarchyInRect:window.bounds afterScreenUpdates:YES];
        } else {
            [window.layer renderInContext:context];
        }
        CGContextRestoreGState(context);
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - Initialzers

- (id)initWithMenus:(NSArray *)menuObjects andNavigationController:(UINavigationController *)navigationController {
    self = [super init];
    if (self) {
        self.menus = menuObjects;
        self.navigation = navigationController;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    index = [NSNumber numberWithInt:0];
    self.view.backgroundColor = [UIColor blackColor];
	UIImage *screenshot = [self screenshot];
    
    //Create Views
    UIButton *dismissButton = [[UIButton alloc] initWithFrame:self.view.frame];
    _titleLabel = [[UILabel alloc] init];
    _backImageView = [[UIImageView alloc] initWithImage:screenshot];
    
    //Set View Attributes
    [dismissButton addTarget:self action:@selector(slideOut) forControlEvents:UIControlEventTouchDown];
    dismissButton.backgroundColor = [UIColor clearColor];
    _titleLabel.text = @"RPI Mobile";
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = [UIFont systemFontOfSize:35];
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake((self.view.frame.size.width-_titleLabel.frame.size.width)/2, 40, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    _titleLabel.alpha = 0;
    
    //Add Views
    [self.view addSubview:_backImageView];
    [self.view insertSubview:dismissButton aboveSubview:_backImageView];
    [self.view insertSubview:_titleLabel aboveSubview:_backImageView];
    
    //UICollectionView
    _layout = [[UICollectionViewFlowLayout alloc] init];
    //[_layout setItemSize:CGSizeMake(150, 180)];
    [_layout setItemSize:CGSizeMake(120, 144)];
    [_layout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [_layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _layout.minimumLineSpacing = 100;
    _layout.minimumInteritemSpacing = 0;
    
    //_collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(-145, 110-60, self.view.frame.size.width+60, self.view.frame.size.height-110+40) collectionViewLayout:_layout];
    float collectionHeight = self.view.frame.size.height-110-40;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(30, (self.view.frame.size.height-collectionHeight)-30, self.view.frame.size.width-60, self.view.frame.size.height-110-40) collectionViewLayout:_layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.scrollEnabled = NO;
    _collectionView.alpha = 0;
    [self.view insertSubview:_collectionView aboveSubview:dismissButton];
    [_collectionView registerClass:[RPIMasterCollectionCell class] forCellWithReuseIdentifier:@"MasterCell"];
}

#pragma mark - View Handling

- (void)slideOut {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [UIView animateWithDuration:0.2 animations:^(void) {
        [_frontImageView removeFromSuperview];
        _titleLabel.alpha = 0;
        _collectionView.alpha = 0;
        [_backImageView setFrame:self.view.frame];
    }
                     completion:^ (BOOL finished)
     {
         if (finished) {
             [self.view removeFromSuperview];
         }
     }];
}

- (void)slideIn {
    [UIView animateWithDuration:.2 animations:^(void) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        [_backImageView setFrame:CGRectMake(15, 30, _backImageView.frame.size.width-30, _backImageView.frame.size.height-60)];
    }
    completion:^ (BOOL finished) {
         if (finished) {
             UIImage *screenshot = [self screenshot];
             //screenshot = [screenshot applyDarkEffect];
             screenshot = [screenshot applyBlurWithRadius:1.5 tintColor:[UIColor clearColor] saturationDeltaFactor:1 maskImage:nil];
             _frontImageView = [[UIImageView alloc] initWithImage:screenshot];
             _frontImageView.alpha = 0;
             [self.view insertSubview:_frontImageView aboveSubview:_backImageView];
             [UIView animateWithDuration:.3 animations:^(void) {
                _frontImageView.alpha = 1;
                _titleLabel.alpha = 1;
                 _collectionView.alpha = 1;
                //_layout.itemSize = CGSizeMake(75, 90);
                //_collectionView.frame = CGRectMake(15, 110, self.view.frame.size.width-30, self.view.frame.size.height-110-40);
                //[_layout invalidateLayout];
                //_layout.minimumLineSpacing = 25;
                //_layout.minimumInteritemSpacing = 25;
             }];
         }
     }];
    /*UIImage *screenshot = [self screenshot];
    //screenshot = [screenshot applyDarkEffect];
    screenshot = [screenshot applyBlurWithRadius:1.8 tintColor:[UIColor clearColor] saturationDeltaFactor:1 maskImage:nil];
    _frontImageView = [[UIImageView alloc] initWithImage:screenshot];
    _frontImageView.alpha = 0;
     [self.view insertSubview:_frontImageView aboveSubview:_backImageView];
    [UIView animateWithDuration:.3 animations:^(void) {
        _frontImageView.alpha = 1;
        _titleLabel.alpha = 1;
        _collectionView.alpha = 1;
    }];*/
}

- (void)show {
    [self.view setFrame:[[UIScreen mainScreen] bounds]];
    UIImage *screenshot = [self screenshot];
    [_backImageView setImage:screenshot];
    [navigation.view addSubview:self.view];
    [[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:[index intValue] inSection:0]] setSelected:true];
    [self slideIn];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return menus.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"MasterCell";
    RPIMasterCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    cell.menuObject = (RPIMasterMenuObject *)[self.menus objectAtIndex:indexPath.row];
    if (indexPath.row == [index intValue]) {
        [cell setSelected:TRUE];
    }
    
    return cell;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    index = [NSNumber numberWithInteger:indexPath.row];
    RPIMasterMenuObject *menu = (RPIMasterMenuObject *)[self.menus objectAtIndex:[index integerValue]];
    [navigation setViewControllers:@[menu.viewController]];
    [self slideOut];
}

@end
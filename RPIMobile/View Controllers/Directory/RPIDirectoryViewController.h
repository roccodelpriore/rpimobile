//
//  RPIDirectoryViewController.h
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/27/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPIMasterViewController.h"

@interface RPIDirectoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

- (id)initWithMaster:(RPIMasterViewController *)masterController;

@end
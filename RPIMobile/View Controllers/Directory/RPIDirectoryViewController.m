//
//  RPIDirectoryViewController.m
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/27/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Realm/Realm.h>
#import "RPIDirectoryViewController.h"
#import "RPIDirectoryResultsController.h"
#import "MMDrawerBarButtonItem.h"
#import "RLMPerson.h"

static NSString *cellIdentifier = @"directoryCell";

@interface RPIDirectoryViewController () {
    RPIMasterViewController *master;
    UISearchController      *searchController;
    RPIDirectoryResultsController *resultsController;
    
    UISearchBar *searchBar;
    UITableView *tableView;
    
    RLMRealm *defaultRealm;
    RLMResults *recentHistory;
}

@end

@implementation RPIDirectoryViewController

#pragma mark - Initialzers

- (id)initWithMaster:(RPIMasterViewController *)masterController {
    self = [super init];
    if (self) {
        master = masterController;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:animated];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    //self.navigationController.navigationBar.translucent = NO;
    
    //Static Values
    //float top = [UIApplication sharedApplication].statusBarFrame.size.height+self.navigationController.navigationBar.frame.size.height;
    float top = 0;
    float height = self.view.frame.size.height;
    float width = self.view.frame.size.width;
    float searchBarHeight = 44;
    float syncButtonHeight = 80;
    float tableViewHeight = height-syncButtonHeight-searchBarHeight-5;
    float historyLabelHeight = 26;
    
    //Realm Data
    defaultRealm = [RLMRealm defaultRealm];
    recentHistory = [RLMPerson allObjects];
    
    //Set View Attributes
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.828 green:0.000 blue:0.000 alpha:1.000]];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.9490 green:0.9569 blue:0.9608 alpha:1.0]];
    
    //Set Navigation Bar Items
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:master action:@selector(show)];
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:searchButton];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    [self.navigationItem setTitle:@"Directory"];
    
    //UISearchBar
    resultsController = [[RPIDirectoryResultsController alloc] init];
    searchController = [[UISearchController alloc] initWithSearchResultsController:resultsController];
    searchBar = searchController.searchBar;
    [searchController setSearchResultsUpdater:resultsController];
    [searchBar setDelegate:self];
    [searchBar setFrame:CGRectMake(0, top, width, searchBarHeight)];
    
    //UITableView
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, searchBarHeight+top, width, tableViewHeight) style:UITableViewStyleGrouped];
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [tableView setBackgroundColor:self.view.backgroundColor];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    
    //Sync Button
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, searchBarHeight+tableViewHeight+5+top, width, syncButtonHeight)];
    [button setTitle:@"Sync" forState:UIControlStateNormal];
    [button.titleLabel setTextColor:[UIColor blueColor]];
    [button setBackgroundColor:[UIColor clearColor]];
    
    //No History Label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (height/2)-(historyLabelHeight/2), width, historyLabelHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor darkGrayColor]];
    [label setFont:[UIFont systemFontOfSize:22]];
    [label setText:@"No Recent History"];
    
    //Add SubViews
    [self.view addSubview:searchBar];
    [self.view insertSubview:tableView belowSubview:searchBar];
    [self.view insertSubview:button belowSubview:tableView];
    [self.view insertSubview:label belowSubview:button];
}

- (void)searchAction {
    [searchBar becomeFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar {
    [aSearchBar resignFirstResponder];
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    BOOL hidden = FALSE;
    if (recentHistory.count == 0) {
        hidden = TRUE;
    }
    
    [UIView animateWithDuration:.25 animations:^(void) {
        [tableView setHidden:hidden];
    }];
    
    return recentHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end

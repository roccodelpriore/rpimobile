//
//  RPIDirectoryResultsController.m
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/28/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <Realm/Realm.h>
#import "RPIDirectoryResultsController.h"
#import "RLMPerson.h"

@interface RPIDirectoryResultsController () {
    RLMResults        *results;
    NSOperationQueue  *queue;
    NSTimer           *searchTimer;
    NSString          *searchString;
    NSString          *lastString;
    BOOL               textChanged;
}

@end

static NSString *cellIdentifier = @"resultsCell";
//  Base search URL
const NSString *SEARCH_URL = @"http://rpidirectory.appspot.com/api?q=";
//  0.5 seconds
const NSTimeInterval SEARCH_INTERVAL = 0.5f;

@implementation RPIDirectoryResultsController

#pragma mark - Initialzers

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    
    searchTimer = nil;
    queue = [[NSOperationQueue alloc] init];
    queue.name = @"com.rpimobile.RPI-Directory.search";
    queue.maxConcurrentOperationCount = 1;
}

#pragma mark - UISearchResultsUpdating

//  Asynchronously search for people with the current query.
- (void)search
{
    [queue cancelAllOperations];
    
    [queue addOperationWithBlock:^{
        NSError *err = nil;
        NSString *query = [searchString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        if (query == nil) {
            return;
        }
        NSString *searchUrl = [SEARCH_URL stringByAppendingString:query];
        NSString *resultString = [NSString stringWithContentsOfURL:[NSURL URLWithString:searchUrl]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&err];
        
        if (err != nil) {
            NSLog(@"Error retrieving search results for string: %@", searchString);
        } else {
            NSData *resultData = [resultString dataUsingEncoding:NSUTF8StringEncoding];
            id temp_results = [NSJSONSerialization JSONObjectWithData:resultData
                                                         options:NSJSONReadingMutableLeaves
                                                           error:&err];
            
            if (temp_results && [temp_results isKindOfClass:[NSDictionary class]]) {
                NSMutableArray *people = [NSMutableArray array];
                NSMutableArray *searchArray = [temp_results objectForKey:@"data"];
                
                for (NSDictionary *personDict in [temp_results objectForKey:@"data"]) {
                    NSLog(@"PersonDict: %@", personDict);
                }
            }
        }
    }];
}

- (void)searchTimerFunc {
    searchTimer = nil;
    
    if (![lastString isEqualToString:searchString]) {
        searchString = lastString;
        [self search];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    if (searchString == nil && ![lastString isEqualToString:@""]) {
        //  Search
        searchString = searchText;
        [self search];
        
        searchTimer = [NSTimer scheduledTimerWithTimeInterval:SEARCH_INTERVAL
                                                         target:self
                                                       selector:@selector(searchTimerFunc)
                                                       userInfo:nil
                                                        repeats:NO];
    }
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return results.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}

@end

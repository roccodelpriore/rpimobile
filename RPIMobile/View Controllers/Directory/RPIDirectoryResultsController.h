//
//  RPIDirectoryResultsController.h
//  RPIMobile
//
//  Created by Rocco Del Priore on 11/28/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPIDirectoryResultsController : UITableViewController <UISearchResultsUpdating>

@end

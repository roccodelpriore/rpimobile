//
//  RPIShuttleMapViewController.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "RPIMasterViewController.h"
#import "RPIShuttleDataManager.h"
#import "RPIShuttle.h"
#import "Route.h"
#import "Stop.h"
#import "MKRoutePolyline.h"

@interface RPIShuttleMapViewController : UIViewController <MKMapViewDelegate>
  @property (nonatomic) MKMapView* mapView;
  @property (nonatomic) NSMutableArray* routeLines;
  @property (nonatomic) NSMutableArray* routeLineViews;
  @property (nonatomic) NSMutableDictionary* routesAndStops;
  @property (nonatomic) NSMutableDictionary* currentShuttles;
  @property (nonatomic) RPIShuttleDataManager* dataManager;
  @property (nonatomic) NSTimer *dataUpdateTimer;

- (id)initWithMaster:(RPIMasterViewController *)masterController;
@end

@interface UIColor (stringcolor)

+ (UIColor*)UIColorFromRGBString:(NSString *)rgbString;
+ (UIColor*)UIColorFromRGBAString:(NSString *)rgbaString;

@end
//
//  RPIShuttleMapViewController.m
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import "RPIShuttleMapViewController.h"
#import "MMDrawerBarButtonItem.h"

const NSTimeInterval UPDATE_THRESHOLD = -180.0f;    //  3 minutes

@interface RPIShuttleMapViewController () {
    RPIMasterViewController *master;
    MKCoordinateRegion region;
}
@end

@implementation RPIShuttleMapViewController

- (id)initWithMaster:(RPIMasterViewController *)masterController {
    self = [super init];
    if (self) {
        master = masterController;
      
        self.routeLines = [[NSMutableArray alloc] init];
        self.routeLineViews = [[NSMutableArray alloc] init];
        self.routesAndStops = [[NSMutableDictionary alloc] init];
        self.dataManager = [[RPIShuttleDataManager alloc] init];
      
        region.center.latitude = 42.7312;
        region.center.longitude = -73.6753125;
        region.span.latitudeDelta = 0.0230;
        region.span.longitudeDelta = 0.0274;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated  {
    [super viewDidAppear:animated];
    
    [self.navigationController setToolbarHidden:NO animated:YES];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  //Set Up Navigation
  MMDrawerBarButtonItem* leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:master action:@selector(show)];
  [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.828 green:0.000 blue:0.000 alpha:1.000]];
  [self.navigationController.toolbar setBarTintColor:[UIColor colorWithRed:0.828 green:0.000 blue:0.000 alpha:1.000]];
  [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
  [self.navigationItem setTitle:@"Shuttle"];
  self.navigationController.toolbarHidden = NO;
  
  //Set Up MapView
  self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
  [self.mapView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
  [self.mapView setRotateEnabled:FALSE];
  [self.mapView setPitchEnabled:FALSE];
  [self.mapView setDelegate:self];
  [self.mapView setRegion:region];
  
  //Add Toolbar Items
  MKUserTrackingBarButtonItem *locationButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
  self.toolbarItems = @[locationButton];
  
  //Add SubViews
  [self.view addSubview:self.mapView];

  // Load routes and stops from API
  [self.dataManager loadRoutesAndStops: self.routesAndStops];

  // Take notice when the routes and stops are updated.
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(managedRoutesLoaded)
                                               name:RoutesAndStopsLoaded
                                             object:nil];
  //  Take notice when vehicles are updated.
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifyVehiclesUpdated:)
                                               name:VehiclesUpdated
                                             object:nil];
  
  // Schedule a timer to make the DataManager pull new data every 5 seconds
  self.dataUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                                          target:self.dataManager
                                                        selector:@selector(updateVehicleData)
                                                        userInfo:nil
                                                         repeats:YES];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

//  Add the overlay for the route to the map view, and create a shuttle image with
//  a color matching the route's color
- (void)addRoute:(Route*)route {
  //  UIImage *coloredImage;
  NSString *pointsString = route.pointList;
  NSArray *coords = [pointsString componentsSeparatedByString:@";"];
  
  // No points on this route, so can't add it. Sanity check
  if (!coords || [coords count] == 0) {
    return;
  }
  
  CLLocationCoordinate2D* points = malloc([coords count] * sizeof(CLLocationCoordinate2D));
  
  int counter = 0;
  for (NSString *coord in coords) {
    NSArray *coordLatAndLong = [coord componentsSeparatedByString:@","];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(
                                          [[coordLatAndLong objectAtIndex:0] doubleValue],
                                          [[coordLatAndLong objectAtIndex:1] doubleValue]);
    
    points[counter] = coordinate;
    counter++;
  }
  
  MKRoutePolyline *polyLine = [MKRoutePolyline polylineWithCoordinates:points count:counter];
  polyLine.width = route.width;
  polyLine.color = route.color;
  
  [self.routeLines addObject:polyLine];
  [self.mapView addOverlay:polyLine];
  
  //  Create the colored shuttle image for the route
//  if (routeView.fillColor) {
//    coloredImage = [[self.magentaShuttleImages objectForKey:@"west"] copyMagentaImageasColor:routeView.fillColor];
//    [[self.shuttleImages objectForKey:@"east"] setValue:coloredImage
//                                                 forKey:[route.routeId stringValue]];
//    
//    coloredImage = [[self.magentaShuttleImages objectForKey:@"north"] copyMagentaImageasColor:routeView.fillColor];
//    [[self.shuttleImages objectForKey:@"north"] setValue:coloredImage
//                                                  forKey:[route.routeId stringValue]];
//    
//    coloredImage = [[self.magentaShuttleImages objectForKey:@"west"] copyMagentaImageasColor:routeView.fillColor];
//    [[self.shuttleImages objectForKey:@"west"] setValue:coloredImage
//                                                 forKey:[route.routeId stringValue]];
//    
//    coloredImage = [[self.magentaShuttleImages objectForKey:@"south"] copyMagentaImageasColor:routeView.fillColor];
//    [[self.shuttleImages objectForKey:@"south"] setValue:coloredImage
//                                                  forKey:[route.routeId stringValue]];
//  }
}

- (void)addStop:(Stop*)stop {
  double latitude;
  double longitude;
  CLLocationCoordinate2D coordinate;
  
  latitude = [stop.latitude doubleValue];
  longitude = [stop.longitude doubleValue];
  coordinate = CLLocationCoordinate2DMake(latitude, longitude);

  MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
  annotation.coordinate = coordinate;
  annotation.title = stop.name;
  [self.mapView addAnnotation: annotation];
}

//- (void)addVehicle:(RPIShuttle*)vehicle {
//  RPIShuttle *newVehicle = [[RPIShuttle alloc] init];
//  
//  newVehicle.coordinate = vehicle.coordinate;
//  newVehicle.heading = vehicle.heading;
////  newVehicle.routeId = vehicle.routeId;
//  [newVehicle setUpdateTime:vehicle.updateTime
//              withFormatter:self.dataManager.timeDisplayFormatter];
//  newVehicle.name = vehicle.name;
//  
//  [self.mapView addAnnotation:newVehicle];
//  [self.currentShuttles setObject:newVehicle forKey:newVehicle.name];
//}

// The routes and stops were loaded in the dataManager
- (void)managedRoutesLoaded {
  NSMutableArray* routes = [self.routesAndStops objectForKey:@"routes"];
  
  if (routes == nil)
  {
    // Deal with error
    NSLog(@"ROUTES == nil");
  } else if ([routes count] > 0) {
    for (Route* route in routes) {
      [self addRoute: route];
    }
  } else {
    //  No routes, so do nothing
    NSLog(@"NO ROUTES");
  }
  
  //  Get all stops
  NSMutableArray* stops = [self.routesAndStops objectForKey:@"stops"];
  
  if (routes == nil)
  {
    // Deal with error
    NSLog(@"STOPS == nil");
  } else if ([routes count] > 0) {
    for (Stop *stop in stops) {
      [self addStop: stop];
    }
  } else {
    //  No routes, so do nothing
    NSLog(@"NO STOPS");
  }
}

//- (void)updateImageForAnnotation:(RPIShuttle*)annotation
//{
//  MKAnnotationView* annotationView = annotation.annotationView;
//  annotationView.image = [annotationView.image imageRotatedByDegrees:annotation.heading];
//}

- (void)notifyVehiclesUpdated:(NSNotification *)notification {
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    NSDictionary *shuttles = [notification.userInfo objectForKey:@"shuttles"];
    
    for (RPIShuttle *shuttle in [shuttles objectEnumerator]) {
      BOOL alreadyAdded = NO;
      
      for (id annotation in self.mapView.annotations) {
        if ([annotation isKindOfClass:[RPIShuttle class]]) {
          if ([[(RPIShuttle*)annotation identifier] isEqualToNumber:shuttle.identifier]) {
            [(RPIShuttle*)annotation copyStatusFromShuttle:shuttle];
//            [self updateImageForAnnotation:annotation];
            
            alreadyAdded = YES;
            break;
          }
        }
      }
      
      if (!alreadyAdded && [shuttle.updateTime timeIntervalSinceNow] > UPDATE_THRESHOLD) {
        [self.mapView addAnnotation:shuttle];
      }
    }
  }];
}

- (MKOverlayRenderer*)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
  if ([overlay isKindOfClass:[MKRoutePolyline class]]) {
    MKRoutePolyline *route = overlay;
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:route];
    
    renderer.lineWidth = [route.width intValue];
    renderer.fillColor = [UIColor UIColorFromRGBString:route.color];
    renderer.strokeColor = renderer.fillColor;
    
    return renderer;
  } else {
    return nil;
  }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
  if ([annotation isKindOfClass:[RPIShuttle class]]) {
    RPIShuttle* shuttleAnnotation = (RPIShuttle*) annotation;
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"ShuttleAnnotation"];
    
    if (annotationView == nil) {
      annotationView = shuttleAnnotation.annotationView;
    } else {
      annotationView.annotation = annotation;
    }
    
    return annotationView;
  } else {
    MKAnnotationView *annotationView=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
    [annotationView setImage:[UIImage imageNamed:@"stop_marker"]];
    return annotationView;
  }
}

@end

// Taken from https://github.com/AbstractedSheep/Shuttle-Tracker/blob/master/ios/Shuttle-Tracker/STMapViewController.m
@implementation UIColor (stringcolor)

//  Take an NSString formatted as RRGGBB and return a UIColor
//  Note that this removes any '#' characters from rgbString
//  before doing anything.
+ (UIColor *)UIColorFromRGBString:(NSString *)rgbString {
  NSScanner *scanner;
  unsigned int rgbValue;
  
  NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"#"];
  rgbString = [rgbString stringByTrimmingCharactersInSet:charSet];
  
  if (rgbString) {
    scanner = [NSScanner scannerWithString:rgbString];
    [scanner scanHexInt:&rgbValue];
    
  } else {
    rgbValue = 0;
  }
  
  //  From the JSON, color comes as RGB
  UIColor *colorToReturn = [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0
                                           green:((float)((rgbValue & 0xFF00) >> 8))/255.0
                                            blue:((float)((rgbValue & 0xFF)))/255.0
                                           alpha:1];
  
  return colorToReturn;
}

//  Take an NSString formatted as such: RRGGBBAA and return a UIColor
+ (UIColor *)UIColorFromRGBAString:(NSString *)rgbaString {
  NSScanner *scanner;
  unsigned int rgbaValue;
  
  if (rgbaString) {
    scanner = [NSScanner scannerWithString:rgbaString];
    [scanner scanHexInt:&rgbaValue];
    
  } else {
    rgbaValue = 0;
  }
  
  //  Assume ABGR format and convert appropriately
  UIColor *colorToReturn = [UIColor colorWithRed:((float)((rgbaValue & 0xFF)))/255.0
                                           green:((float)((rgbaValue & 0xFF00) >> 8))/255.0
                                            blue:((float)((rgbaValue & 0xFF0000) >> 16))/255.0
                                           alpha:((float)((rgbaValue & 0xFF000000) >> 24))/255.0];
  
  return colorToReturn;
}

@end

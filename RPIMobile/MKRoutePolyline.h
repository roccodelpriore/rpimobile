//
//  MKRoutePolyline.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKRoutePolyline : MKPolyline

@property (nonatomic) NSString *color;
@property (nonatomic) NSNumber *width;

@end

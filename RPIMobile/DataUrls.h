//
//  DataUrls.h
//  RPIMobile
//
//  Created by Damian Mastylo on 12/9/14.
//  Copyright (c) 2014 Rensselaer Polytechnic Institute. All rights reserved.
//

#ifndef RPIMobile_DataUrls_h
#define RPIMobile_DataUrls_h

#define RoutesAndStopsUrl @"http://shuttles.rpi.edu/displays/netlink.js"
#define CurrentShuttlesUrl @"http://shuttles.rpi.edu/vehicles/current.js"

#endif

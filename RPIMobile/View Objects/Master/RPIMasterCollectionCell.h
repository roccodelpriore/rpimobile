//
//  MasterCollectionCell.h
//  RPIMobile
//
//  Created by Rocco Del Priore on 5/24/14.
//  Copyright (c) 2014 Stephen Silber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPIMasterMenuObject.h"

@interface RPIMasterCollectionCell : UICollectionViewCell {
    UILabel *_titleLabel;
    UIImageView *_imageView;
    UIButton *_circle;
}

@property (nonatomic) RPIMasterMenuObject *menuObject;

@end
